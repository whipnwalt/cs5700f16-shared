import org.junit.Test;
import Messages.*;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by elascano on 9/21/2016.
 */
public class TickerMessageTester {


    @Test
    public void TickerMessage_TestEncode() throws Exception {

        TickerMessage m1 = new TickerMessage("ABCD", new Date().getTime(), 1001,1002, 1003, 1004, 1005, 1006, 1007);

        assertNotNull(m1);
        byte[] encodedMessage = m1.Encode();
        assertNotNull(encodedMessage);
        assertEquals(42, encodedMessage.length);

        TickerMessage m2 = TickerMessage.Decode(encodedMessage);
        assertNotNull(m2);
        assertEquals(m1.getSymbol(), m2.getSymbol());
        assertEquals(m1.getOpeningPrice(), m2.getOpeningPrice());
        assertEquals(m1.getPreviousClosingPrice(), m2.getPreviousClosingPrice());
        assertEquals(m1.getCurrentPrice(), m2.getCurrentPrice());
        assertEquals(m1.getAskPrice(), m2.getAskPrice());
        assertEquals(m1.getBidPrice(), m2.getBidPrice());
        assertEquals(m1.getCurrentVolume(), m2.getCurrentVolume());
        assertEquals(m1.getAverageVolume(), m2.getAverageVolume());
    }

    @Test
    public void TickerMessage_TestEncodeWithBigInts() throws Exception {

        TickerMessage m1 = new TickerMessage("ABCD", new Date().getTime(), 123456789, 234567890, 345678901, 456789012, 567890123, 678901234, 789012345);

        assertNotNull(m1);
        byte[] encodedMessage = m1.Encode();
        assertNotNull(encodedMessage);
        assertEquals(42, encodedMessage.length);

        TickerMessage m2 = TickerMessage.Decode(encodedMessage);
        assertNotNull(m2);
        assertEquals(m1.getSymbol(), m2.getSymbol());
        assertEquals(m1.getOpeningPrice(), m2.getOpeningPrice());
        assertEquals(m1.getPreviousClosingPrice(), m2.getPreviousClosingPrice());
        assertEquals(m1.getCurrentPrice(), m2.getCurrentPrice());
        assertEquals(m1.getAskPrice(), m2.getAskPrice());
        assertEquals(m1.getBidPrice(), m2.getBidPrice());
        assertEquals(m1.getCurrentVolume(), m2.getCurrentVolume());
        assertEquals(m1.getAverageVolume(), m2.getAverageVolume());
    }

    @Test
    public void TickerMessage_TestEncodingOfEmptyMessage() throws Exception {

        TickerMessage m1 = new TickerMessage();
        byte[] encodedMessage = m1.Encode();
        assertNotNull(encodedMessage);
        assertEquals(42, encodedMessage.length);
        for (int i = 0; i < 6; i++)
            assertEquals(32, encodedMessage[i]);
        for (int i = 6; i < 42; i++)
            assertEquals(0, encodedMessage[i]);
    }

    @Test
    public void TickerMessage_TestDecodingOfBadBytes() throws Exception {

        TickerMessage m = TickerMessage.Decode(null);
        assertNull(m);

        byte[] bytes = new byte[] { };
        m = TickerMessage.Decode(bytes);
        assertNull(m);

        bytes = new byte[] { 65, 66, 67 };
        m = TickerMessage.Decode(bytes);
        assertNull(m);

        bytes = new byte[] { 65, 66, 67, 68, 69, 70, 0, 0 ,0, 0 };
        m = TickerMessage.Decode(bytes);
        assertNull(m);

        bytes = new byte[] { 65, 66, 67, 68, 69, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        m = TickerMessage.Decode(bytes);
        assertNull(m);
    }

}