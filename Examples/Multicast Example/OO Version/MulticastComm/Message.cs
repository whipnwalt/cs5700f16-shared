﻿using System.Text;

namespace MulticastComm
{
    public class Message
    {
        public string Text { get; set; }

        public byte[] Encode()
        {
            if (Text==null) return new byte[0];

            return Encoding.Unicode.GetBytes(Text);
        }

        public static Message Decode(byte[] bytes)
        {
            if (bytes == null) return null;

            string text = Encoding.Unicode.GetString(bytes);
            if (text==FinalMessage.EndOfMessagesText)
                return new FinalMessage();

            return new Message() {Text = text};
        }
    }
}
