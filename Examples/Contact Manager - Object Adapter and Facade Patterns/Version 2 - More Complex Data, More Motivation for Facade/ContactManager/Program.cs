﻿using System;
using System.Windows.Forms;

using AppLayer;

namespace ContactManager
{
    static class Program
    {
        private static string myNameDataFile = "names.csv";
        private static string myPhoneDataFile = "phones.csv";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Use a PersonData facade to load name and phone data, and provide a data source for the grid in the main form
            PersonData personData = new PersonData() { NameFilename = myNameDataFile, PhoneFilename = myPhoneDataFile};

            MainForm form = new MainForm() { DataSource = personData.Data };
            Application.Run(form);

            if (form.DialogResult == DialogResult.OK)
                personData.Save();
        }

    }

}
