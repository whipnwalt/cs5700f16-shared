﻿using System.Collections.Generic;
using System.Linq;

namespace AppLayer
{
    public class Person
    {
        public Name Name { get; set; } = new Name();

        public int Id
        {
            get { return Name.Id; }
            set { Name.Id = value; }
        }

        public string FirstName
        {
            get { return Name.FirstName; }
            set { Name.FirstName = value; }
        }

        public string LastName
        {
            get { return Name.LastName; }
            set { Name.LastName = value; }            
        }

        public List<Phone> PhoneNumbers { get; set; } = new List<Phone>();
        public string Phone
        {
            get
            {
                return string.Join(", ", PhoneNumbers.Select(p => p.PhoneNumber));
            }
            set
            {
                PhoneNumbers.Clear();
                if (!string.IsNullOrEmpty(value))
                {
                    string[] numbers = value.Split(',');
                    foreach (string number in numbers)
                    {
                        if (!string.IsNullOrWhiteSpace(number))
                            PhoneNumbers.Add(new Phone() { PersonId = Id, PhoneNumber = number.Trim() });
                    }
                }
            }
        }

        public void AddPhoneNumbers(IEnumerable<Phone> numbers)
        {
            if (numbers == null) return;

            foreach(Phone p in numbers)
                PhoneNumbers.Add(p);
        }
    }
}
