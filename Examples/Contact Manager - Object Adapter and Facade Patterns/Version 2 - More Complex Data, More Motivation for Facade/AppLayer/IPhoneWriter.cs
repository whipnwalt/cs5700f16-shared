﻿

namespace AppLayer
{
    public interface IPhoneWriter
    {
        void Write(Phone phone);
        void Close();
    }
}
