﻿namespace AppLayer
{
    interface INameWriter
    {
        void Write(Name person);
        void Close();
    }
}
