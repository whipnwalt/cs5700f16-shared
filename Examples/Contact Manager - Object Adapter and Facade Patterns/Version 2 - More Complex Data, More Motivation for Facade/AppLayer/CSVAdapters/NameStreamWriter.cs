﻿using System.IO;

namespace AppLayer.CSVAdapters
{
    public class NameStreamWriter : INameWriter
    {
        private readonly StreamWriter _writer;

        public NameStreamWriter(string filename)
        {
            try
            {
                _writer = new StreamWriter(filename);
            }
            catch
            {
                // ignored
            }
        }

        public void Write(Name person)
        {
            if (_writer != null && person != null)
            {
                string firstName = person.FirstName.Replace(",", "&comma&");
                string lastName = person.LastName.Replace(",", "&comma&");
                _writer.WriteLine("{0},{1},{2}", person.Id, firstName, lastName);
            }
        }

        public void Close()
        {
            _writer?.Close();
        }
    }
}
