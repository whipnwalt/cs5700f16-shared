﻿
namespace AppLayer
{
    public interface IPhoneReader
    {
        bool AtEnd { get; }
        Phone Read();
        void Close();
    }
}
