﻿using System;
using System.Windows.Forms;

using AppLayer;

namespace ContactManager
{
    static class Program
    {
        private static string myDataFile = "persons.csv";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            PersonData personData = new PersonData() { Filename = myDataFile };

            MainForm form = new MainForm() { DataSource = personData.Data };
            Application.Run(form);

            if (form.DialogResult == DialogResult.OK)
                personData.Save();
        }

    }

}
