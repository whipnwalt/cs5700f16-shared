﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Expressions.Operators;

namespace Expressions
{
    public class BinaryOperation : IExpression
    {
        public BinaryOperator Operator { get; set; }
        public IExpression Operand1
        {
            get { return (subExpressions.Count > 0) ? subExpressions[0] : null; }
            set { AddOperand(value, 1); }
        }
        public IExpression Operand2
        {
            get { return (subExpressions.Count > 1) ? subExpressions[1] : null; }
            set { AddOperand(value, 2); }
        }

        public override double Evaluate(Interpretation interpretation)
        {
            double result = 0;
            if (Operator != null && Operand1 != null && Operand2 != null)
            {
                double op1 = Operand1.Evaluate(interpretation);
                double op2 = Operand2.Evaluate(interpretation);
                result = Operator.Execute(op1, op2);
            }
            return result;
        }

        public override double EstimateTime(Interpretation interpretation)
        {
            return 1;
        }

        public override double EstimateValue(Interpretation interpretation)
        {
            return Evaluate(interpretation);
        }

        public override string PrefixNotation
        {
            get { return string.Format("{0} {1} {2}", Operator.Label, Operand1.PrefixNotation, Operand2.PrefixNotation); }
        }

        public override string InfixNotation
        {
            get { return string.Format("({0} {1} {2})", Operand1.PrefixNotation, Operator.Label, Operand2.PrefixNotation); }
        }

        public override string PostfixNotation
        {
            get { return string.Format("{0} {1} {2}", Operand1.PrefixNotation, Operand2.PrefixNotation, Operator.Label); }
        }

        public override IExpression OptimizedExpression
        {
            get
            {
                IExpression result = null;
                if (Operand1 is Constant && Operand2 is Constant)
                {
                    Interpretation i = new Interpretation();
                    result = new Constant()
                    {
                        Value = Operand1.Evaluate(i) + Operand2.Evaluate(i)
                    };
                }
                else
                {
                    IExpression newOperand1 = (Operand1 != null) ? Operand1.OptimizedExpression : null;
                    IExpression newOperand2 = (Operand2 != null) ? Operand2.OptimizedExpression : null;

                    result = new BinaryOperation()
                    {
                        Operand1 = newOperand1,
                        Operand2 = newOperand2,
                        Operator = Operator
                    };
                }
                return result;
            }
        }
    }
}
