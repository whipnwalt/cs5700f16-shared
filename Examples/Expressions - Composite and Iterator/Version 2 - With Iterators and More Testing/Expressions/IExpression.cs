﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Expressions.Iterators;

namespace Expressions
{
    public abstract class IExpression
    {
        protected List<IExpression> subExpressions = new List<IExpression>();

        public abstract double Evaluate(Interpretation interpretation);
        public abstract double EstimateTime(Interpretation interpretation);
        public abstract double EstimateValue(Interpretation interpretation);
        public abstract string PrefixNotation { get; }
        public abstract string InfixNotation { get; }
        public abstract string PostfixNotation { get; }
        public abstract IExpression OptimizedExpression { get; }

        public List<IExpression> SubExpressions { get { return subExpressions; } }

        public Iterator GetPreOrderIterator() { return new PreOrderIterator(this); }
        public Iterator GetInOrderIterator() { return new InOrderIterator(this); }
        public Iterator GetPostOrderIterator() { return new PostOrderIterator(this); }

        protected void AddOperand(IExpression exp, int operandNumber)
        {
            if (exp != null && operandNumber > 0)
            {
                GrowSubExpressions(operandNumber);
                subExpressions[operandNumber - 1] = exp;
            }
        }

        protected void GrowSubExpressions(int targetSize)
        {
            for (int i = subExpressions.Count; i < targetSize; i++)
                subExpressions.Add(null);
        }
    }
}
