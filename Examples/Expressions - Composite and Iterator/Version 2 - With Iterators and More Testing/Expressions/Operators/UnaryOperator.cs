﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressions.Operators
{
    public abstract class UnaryOperator
    {
        public abstract double Execute(double operand);
        public string Label { get { return ToString(); } }
    }
}
