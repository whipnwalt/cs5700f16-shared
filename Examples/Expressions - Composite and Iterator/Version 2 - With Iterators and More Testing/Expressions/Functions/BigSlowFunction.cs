﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Expressions.Functions
{
    /// <summary>
    /// BigSlowFunction
    /// 
    /// This function computes a big number based on one or more numbers, but does slowly.  The actually
    /// mathmatically function is not meaningful.  This function just illustrate a long-running method
    /// whose estimate value is not the same as the real computed value, and the estimated time can be big.
    /// </summary>
    public class BigSlowFunction : Function
    {
        private Random randomGenerator = new Random();

        public override double Execute(List<double> parameters)
        {
            return ComputeBigNumberSlowly(parameters, 0);
        }

        public override double GetEstimatedTime(List<double> parameters)
        {
            double result = 0;
            if (parameters != null)
            {
                double[] counts = new double[parameters.Count+1];
                counts[0] = 1;
                for (int i = 0; i < parameters.Count; i++)
                    counts[i + 1] = counts[i] * parameters[i];

                for (int i = 0; i < counts.Length; i++)
                    result += counts[i];
            }
            return result;
        }

        public override double GetEstimatedValue(List<double> parameters)
        {
            return GetEstimatedTime(parameters) / 2;
        }

        private double ComputeBigNumberSlowly(List<double> parameters, int startingWithIndex)
        {
            double result = 0;
            if (parameters != null)
            {
                if (startingWithIndex < parameters.Count)
                {
                    for (int i = 0; i < parameters[startingWithIndex]; i++)
                    {
                        result += ComputeBigNumberSlowly(parameters, startingWithIndex + 1);
                        Thread.Sleep(1);
                    }
                }
                else
                    result = randomGenerator.NextDouble();
            }
            return result;
        }

    }
}
