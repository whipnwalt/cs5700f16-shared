﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressions.Iterators
{
    public class PreOrderIterator : Iterator
    {
        public PreOrderIterator(IExpression firstNode) : base(firstNode) { }

        public override bool MoveNext()
        {
            if (nodesToVisit.Count == 0)
                currentNode = null;
            else
            {
                // Visit the next node on the stack
                NodeMarker marker = nodesToVisit.Pop();
                currentNode = marker.Node;

                // Add this node's children to the stack in reverse order
                for (int i = currentNode.SubExpressions.Count - 1; i >= 0; i--)
                    nodesToVisit.Push(new NodeMarker() { Node = currentNode.SubExpressions[i] });
            }

            return !IsDone;
        }
    }
}
