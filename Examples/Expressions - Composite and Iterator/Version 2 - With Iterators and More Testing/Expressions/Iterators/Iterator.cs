﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressions.Iterators
{
    public abstract class Iterator
    {
        protected Stack<NodeMarker> nodesToVisit = new Stack<NodeMarker>();
        protected IExpression currentNode;

        public Iterator(IExpression firstNode)
        {
            if (firstNode!=null)
                nodesToVisit.Push(new NodeMarker() { Node = firstNode } );
        }

        public IExpression Current
        {
            get { return currentNode; }
        }

        public bool IsDone { get { return currentNode == null && nodesToVisit.Count == 0; } }

        public abstract bool MoveNext();

        protected class NodeMarker
        {
            public IExpression Node { get; set; }
            public bool HasMarkedChildren { get; set; }
        }
    }
}
