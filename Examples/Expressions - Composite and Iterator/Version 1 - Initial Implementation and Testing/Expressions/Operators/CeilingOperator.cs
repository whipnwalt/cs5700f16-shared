﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressions.Operators
{
    public class CeilingOperator : UnaryOperator
    {
        public override double Execute(double operand)
        {
            return Math.Ceiling(operand);
        }

        public override string ToString()
        {
            return "\u2309";
        }
    }
}
