﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressions
{
    public interface IExpression
    {
        double Evaluate(Interpretation interpretation);
        double EstimateTime(Interpretation interpretation);
        double EstimateValue(Interpretation interpretation);
        string PrefixNotation { get; }
        string InfixNotation { get; }
        string PostfixNotation { get; }
        IExpression OptimizedExpression { get; }
    }
}
