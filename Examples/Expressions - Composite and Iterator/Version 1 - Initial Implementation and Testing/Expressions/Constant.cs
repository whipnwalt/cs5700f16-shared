﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressions
{
    public class Constant : IExpression
    {
        public double Value { get; set; }

        public double Evaluate(Interpretation interpretation)
        {
            return Value;
        }

        public double EstimateTime(Interpretation interpretation)
        {
            return 0;
        }

        public double EstimateValue(Interpretation interpretation)
        {
            return Value;
        }

        public string PrefixNotation
        {
            get { return Value.ToString(); }
        }

        public string InfixNotation
        {
            get { return Value.ToString(); }
        }

        public string PostfixNotation
        {
            get { return Value.ToString(); }
        }

        public IExpression OptimizedExpression
        {
            get { return this.MemberwiseClone() as Constant; }
        }
    }
}
