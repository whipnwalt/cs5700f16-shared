﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Expressions.Operators;

namespace Expressions
{
    public class BinaryOperation : IExpression
    {
        public BinaryOperator Operator { get; set; }
        public IExpression Operand1 { get; set; }
        public IExpression Operand2 { get; set; }

        public double Evaluate(Interpretation interpretation)
        {
            double result = 0;
            if (Operator != null && Operand1 != null && Operand2 != null)
            {
                double op1 = Operand1.Evaluate(interpretation);
                double op2 = Operand2.Evaluate(interpretation);
                result = Operator.Execute(op1, op2);
            }
            return result;
        }

        public double EstimateTime(Interpretation interpretation)
        {
            return 1;
        }

        public double EstimateValue(Interpretation interpretation)
        {
            return Evaluate(interpretation);
        }

        public string PrefixNotation
        {
            get { return string.Format("{0} {1} {2}", Operator.Label, Operand1.PrefixNotation, Operand2.PrefixNotation); }
        }

        public string InfixNotation
        {
            get { return string.Format("({0} {1} {2})", Operand1.PrefixNotation, Operator.Label, Operand2.PrefixNotation); }
        }

        public string PostfixNotation
        {
            get { return string.Format("{0} {1} {2}", Operand1.PrefixNotation, Operand2.PrefixNotation, Operator.Label); }
        }

        public IExpression OptimizedExpression
        {
            get
            {
                IExpression result = null;
                if (Operand1 is Constant && Operand2 is Constant)
                {
                    Interpretation i = new Interpretation();
                    result = new Constant()
                    {
                        Value = Operand1.Evaluate(i) + Operand2.Evaluate(i)
                    };
                }
                else
                {
                    result = new BinaryOperation()
                    {
                        Operand1 = Operand1.OptimizedExpression,
                        Operand2 = Operand2.OptimizedExpression
                    };
                }
                return result;
            }
        }
    }
}
