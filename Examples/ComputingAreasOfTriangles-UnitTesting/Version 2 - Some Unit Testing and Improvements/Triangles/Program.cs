﻿using System;

namespace Triangles
{
    class Program
    {
        static void Main(string[] args)
        {
            Triangle t1 = new Triangle() {Sides = new double?[] {3.0, 4.0, 5.0}};
            Console.WriteLine("t1.Area = {0}", t1.ComputeArea());

            Console.ReadKey();
        }
    }
}
